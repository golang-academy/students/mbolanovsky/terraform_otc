variable image_id {}

resource "opentelekomcloud_compute_instance_v2" "basic" {
  name            = "basic"
  image_id        = "0a9ccafc-47f7-481d-9365-35814ce5d301" #var.image_id
  flavor_id       = "s3.medium.1"
  key_pair        = "public_mb"
  security_groups = ["Golang-academy"]

  network {
    name = "subnet-reskill"
  }

  metadata = {
    this = "mbolanov-node1"
  }

tags = {
    muh = "kuh"
  }
}
